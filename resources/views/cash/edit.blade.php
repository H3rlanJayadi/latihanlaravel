@extends('layout.table')
@section('judul')
    Halaman Tambah cash
@endsection
@section('content')
<form action="/cash/{{$cash->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama cash</label>
      <input type="text" value="{{$cash->nama}}" name="nama" class="form-control"> 
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}></div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" value="{{$cash->umur}}" name="umur"
        class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}></div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$cash->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}></div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection