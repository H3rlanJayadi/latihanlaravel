@extends('layout.table')
@section('judul')
    Halaman List cash
@endsection
@section('content')

<a href="/cash/create" class="btn btn-primary btn-sm my-2">Menambah Cash</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cash as $skey=>$item)
            <tr>
                <td>{{$skey+1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                   
                    <form action="/cash/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cash/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/cash/{{$item->id}}/edit" class="btn btn-sm btn-warning">edit</a>
                    <input type="submit" class="btn btn-sm btn-danger" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            
      <tr>
        <td>data kosong</td>
      </tr>
      
      @endforelse
    </tbody>
  </table>
  @endsection