<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@home');
Route::get('/ register', 'AuthController@registrasi');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/tablee', function(){
    return view('halaman.tablee');
});

//CRUD cash

//create
//form input create  cash
Route::get('/cash/create', 'cashcontroller@create');
//menyimpan data ke database
Route::post('/cash', 'cashcontroller@store');
//menampilkan semua data
Route::get('/cash', 'cashcontroller@index');
//menampilkan detail
Route::get('/cash/{cash_id}', 'cashcontroller@show');
//update data
//form edit data cash
Route::get('/cash/{cash_id}/edit', 'cashcontroller@edit');
//update data berdasarkan id
Route::put('/cash/{cash_id}', 'cashcontroller@update');
//delete
Route::delete ('/cash/{cash_id}' , 'cashcontroller@destroy');