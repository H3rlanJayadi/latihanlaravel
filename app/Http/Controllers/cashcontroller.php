<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class cashcontroller extends Controller
{
    public function create()
    {
        return view('cash.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
    [
    'nama.required' => 'inputan nama harus diisi, tidak boleh kosong',
    'umur.required' => 'inputan umur harus diisi, tidak boleh kosong',
    'bio.required' => 'inputan bio harus diisi, tidak boleh kosong',
    ]
    );
    DB::table('cash')->insert(
        [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
            ]
    );
    return redirect('/cash');
    }
    public function index()
    {
        $cash = DB::table('cash')->get();
       // dd($cash);
        return view('cash.index', \compact('cash'));
    }
    public function show($id)
    {
        $cash = DB::table('cash')->where('id',$id)->first();

        return view('cash.show', compact('cash'));
    }
    public function edit($id)
    {
        $cash = DB::table('cash')->where('id',$id)->first();

        return view('cash.edit', compact('cash'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
    [
    'nama.required' => 'inputan nama harus diisi, tidak boleh kosong',
    'umur.required' => 'inputan umur harus diisi, tidak boleh kosong',
    'bio.required' => 'inputan bio harus diisi, tidak boleh kosong',
    ]
    );
     DB::table('cash')
     ->where('id',$id)
     ->update(
         [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
         ]
         );
         return redirect('/cash');
    }
    public function destroy($id)
    {
        DB::table('cash')->where('id', '=', $id)->delete();

        return redirect('/cash');
    }
}
