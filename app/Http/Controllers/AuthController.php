<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi()
    {
        return view('halaman.registrasi');
    }
    
    public function kirim(Request $request)
    {
       $nama = $request['nama'];
       $alamat = $request['address'];

       return view('halaman.welcomee', compact('nama','alamat'));
    }

}
